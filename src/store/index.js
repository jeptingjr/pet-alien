import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);
const petTypes = {
  crab: {
    baseImage: require("../assets/crab1.png"),
    danceImage: require("../assets/crab2.png"),
    danceMotivation: 60,
  },
  squid: {
    baseImage: require("../assets/squid1.png"),
    danceImage: require("../assets/squid2.png"),
    danceMotivation: 20,
  },
  cyclops: {
    baseImage: require("../assets/cyclops1.png"),
    danceImage: require("../assets/cyclops2.png"),
    danceMotivation: 150,
  }
};
export default new Vuex.Store({
  state: {
    currentPet: {},
    petName: "",
    petType: "",
    petImage: "",
    danceMotivation: 0,
    satiation: 0,
    dancing: false,
    history: []
  },
  mutations: {
    CREATE_NEW_PET(state, newPet) {
      state.petName = newPet.name;
      state.petType = newPet.type;
      state.currentPet = petTypes[newPet.type];
    },
    DISPLAY_PET(state, petType) {
      state.petImage = petTypes[petType].baseImage;
    },
    FEED_PET(state) {
      state.satiation += 10;
      state.history.unshift("FEED");
    },
    REQUEST_DANCE(state) {
      if (state.satiation >= state.currentPet.danceMotivation) {
        state.dancing = true;
        state.petImage = state.currentPet.danceImage;
        state.history.unshift("CAN_DANCE");
      } else {
        state.history.unshift("CANT_DANCE");
      }
    },
    BOOGIE(state) {
      if (state.petImage === state.currentPet.baseImage) {
        state.petImage = state.currentPet.danceImage;
      } else {
        state.petImage = state.currentPet.baseImage;
      }
      state.satiation -= 5;
    },
    STOP(state) {
      state.dancing = false;
      state.petImage = state.currentPet.baseImage;
    },
    RELEASE(state) {
      Object.assign(state, {
        currentPet: {},
        petName: "",
        petType: "",
        petImage: "",
        danceMotivation: 0,
        satiation: 0,
        dancing: false,
        history: [],
      });
    },
  },
  strict: true
});
