## This was probably one of my favorite projects!!

This project was a bug hunt for students. Originally I added 20 bugs to the code and had students investigate what I wrote to fix everything. It was a fun activity that the students clearly enjoyed and definitely was a highlight of my teaching experience. Fortunately I took this project with me when I got laid off, unfortunately I lost the list of all the bugs, so this version is 100% fixed and working.

My favorite part of this project was creating the dance animations, they are cute and worth checking out!

### Explanation

Here we have a pet alien game where we can create either a crab, cyclops, or squid pet. Once our pet is created we can ask our pet to dance, but just like humans our pet needs a little motivation (food) to boogie. If we ask our pet to dance while it is still hungry, the game history should display a 🤬, if our pet is satiated our history should display a 💃, when we feed our pet the history should display a 🍔.

To run this application, just clone the repo then run:
    npm install
    npm run serve


# This uses Vue2!
